from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.db import transaction
from django.forms.utils import ValidationError


from .models import Student, User, Files


class TeacherSignUpForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = User
        fields = ('email', 'username',)

    def save(self, commit=True):
        user = super().save(commit=False)
        user.is_teacher = True
        if commit:
            user.save()
        return user


class StudentCreateForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = ('owner', 'Imie', 'Nazwisko', )


class FilesForm(forms.ModelForm):
    class Meta:
        model = Files
        fields = ('filled', 'file',)


