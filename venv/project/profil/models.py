from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.html import escape, mark_safe


class User(AbstractUser):
    is_teacher = models.BooleanField(default=False)
    is_student = models.BooleanField(default=False)


class Student(models.Model):
    owner = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name='testy', db_constraint=False)
    Imie = models.CharField(max_length=255, default='')
    Nazwisko = models.CharField(max_length=255, default='')
    Email = models.CharField(max_length=255, default='')
    id = models.AutoField(primary_key=True)

    def __str__(self):
        return self.Imie + self.Nazwisko


class Files(models.Model):
    file = models.FileField(upload_to='pdfs/', null=True, blank=True)
    filled = models.ForeignKey(Student, on_delete=models.DO_NOTHING, default='', related_name='fill')
    id = models.AutoField(primary_key=True)

    def __str__(self):
        return str(self.filled) + ": " + str(self.file)
