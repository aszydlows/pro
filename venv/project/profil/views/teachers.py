from django.core.files.base import ContentFile
from django.core.mail import EmailMultiAlternatives
from django.shortcuts import get_object_or_404, redirect, render
from django.contrib import messages
from django.contrib.auth import login
from django.db.models import Avg, Count
from django.views.generic import (CreateView, ListView, UpdateView)
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.urls import reverse, reverse_lazy

from ..decorators import teacher_required
from ..models import Student, User, Files
from ..forms import TeacherSignUpForm, FilesForm


from io import BytesIO, StringIO
from django.template.loader import get_template
from xhtml2pdf import pisa


class TeacherSignUpView(CreateView):
    model = User
    form_class = TeacherSignUpForm
    template_name = 'registration/signup_form.html'

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'teacher'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('teachers:student_list')


@method_decorator([login_required, teacher_required], name='dispatch')
class StudentListView(ListView):
    model = Student
    ordering = ('Imie', 'Nazwisko', )
    context_object_name = 'student'
    template_name = 'profil/teachers/student_list.html'

    def get_queryset(self):
        return Student.objects.filter(owner=self.request.user)


@method_decorator([login_required, teacher_required], name='dispatch')
class StudentCreateView(CreateView):
    model = Student
    fields = ('Imie', 'Nazwisko', 'Email')
    template_name = 'profil/teachers/add_student_form.html'

    def form_valid(self, form):
        student = form.save(commit=False)
        student.owner = self.request.user
        student.save()
        return redirect('teachers:student_list')


@method_decorator([login_required, teacher_required], name='dispatch')
class StudentUpdateView(UpdateView):
    model = Student
    fields = ('Imie', 'Nazwisko', 'Email')
    context_object_name = 'student'
    template_name = 'profil/teachers/student_change.html'

    def get_context_data(self, **kwargs):
        kwargs['testy'] = Files.objects.filter(filled=self.get_object())
        return super().get_context_data(**kwargs)

    def get_queryset(self):
       return self.request.user.testy.all()

    def get_success_url(self):
        return reverse('teachers:student_change', kwargs={'pk': self.object.pk, 'tests': ['ab', 'cd']})


def send_email(request, pk):
    student = get_object_or_404(Student, owner=request.user, id=pk)
    subject = "Analiza AAC"
    from_email, to = request.user.email, student.Email
    text_content = 'Link do formularza: ' \
                   'http://127.0.0.1:8000/formularz .' \
                   ' Podaj dane swojego koordynatora: {} oraz imię i nazwisko: {} {}' .format(request.user, student.Imie, student.Nazwisko)

    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.send()
    messages.success(request, 'Email wysłany')
    return redirect('teachers:student_change', student.pk)


def add_test(request, data, time):

    teacher = get_object_or_404(User, username=data[3])
    student = get_object_or_404(Student, owner=teacher, Imie=data[0], Nazwisko=data[1])

    obj = Files(filled=student)

    template = get_template('invoice.html')
    html = template.render({"data": request.quiz, "info": data})
    result = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("UTF-8")), result)
    content = ContentFile(result.getvalue(), '{}_{}.pdf'.format(student, time))
    result.close()

    obj.file = (content)
    obj.save()
