from django.urls import include, path

from django.conf import settings
from django.conf.urls.static import static


from .views import profil, teachers

urlpatterns = [
    path('', profil.home, name='home'),

    path('teachers/', include(([
        path('', teachers.StudentListView.as_view(), name='student_list'),
        path('student/add/', teachers.StudentCreateView.as_view(), name='add_student'),
        path('student/<int:pk>/', teachers.StudentUpdateView.as_view(), name='student_change'),
        path('student/<int:pk>/add_test', teachers.send_email, name='send_email'),
        #path('student/<int:pk>/', teachers.StudentUpdateView.as_view(), name='student_change'),

    ], 'profil'), namespace='teachers')),
]    + static(settings.MEDIA_URL, document_root= settings.MEDIA_ROOT)
