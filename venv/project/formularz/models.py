from django.db import models
from django import forms

Yes_No = [(True,'Tak'),
          (False, 'Nie')]


class Part(models.Model):
    part_nr = models.IntegerField('Numer części', null=False, blank=False, default=1)
    name = models.CharField(max_length=255)
    class Meta:
        verbose_name = 'Część'
        verbose_name_plural = 'Części'
    def __str__(self):
        return ("Część " + str(self.part_nr) + " - " + str(self.name))

class Yes_No_Category(models.Model):
    message = models.CharField('Komunikat', max_length=255, null=True, blank= True)
    name = models.CharField(max_length=255)
    class Meta:
        verbose_name = 'Kategoria przesiewu'
        verbose_name_plural = 'Kategorie przesiewu'

    def get_questions_list(self):
        return [(question.id, question.text) for question in
                Question_Yes_No.objects.filter(category=self)]


    def __str__(self):
        return str(self.name)


class Question_Open(models.Model):
    text = models.CharField('Pytanie otwarte', max_length=500)
    part = models.ForeignKey(Part, on_delete=models.CASCADE, related_name='parts_open', verbose_name='Część')
    is_main = models.BooleanField('Pytanie główne', default=False)

    def __str__(self):
        return self.text
    class Meta:
        verbose_name = 'Pytanie otwarte'
        verbose_name_plural = 'Pytania otwarte'



class Question_Closed(models.Model):
    text = models.CharField('Pytanie zamknięte', max_length=500)
    part = models.ForeignKey(Part, on_delete=models.CASCADE, related_name='parts_closed', verbose_name='Część')
    is_main = models.BooleanField('Pytanie główne', default=False)
    yes_no = models.ManyToManyField(Yes_No_Category, related_name='yes_no_categories', verbose_name='Przesiew', blank=True, null=True, default=None)

    def get_answers_list(self):
        return [(answer.id, answer.text) for answer in
                Answer_Closed.objects.filter(question_closed=self)]

    class Meta:
        verbose_name = 'Pytanie zamknięte'
        verbose_name_plural = 'Pytania zamknięte'

    def __str__(self):
        return self.text


class Question_Yes_No(models.Model):
    text = models.CharField('Pytanie przesiewowe', max_length=255)
    display_message = models.BooleanField('Wyświetl komunikat jeśli odpowiedź "tak"', default=False)
    category = models.ForeignKey(Yes_No_Category, on_delete=models.CASCADE, related_name='categories', verbose_name='Kategoria', null=True, blank=True, default=None)

    related_question_open = models.ManyToManyField(Question_Open,  blank=True, default=None,
                                               related_name='related_yes_no_open', verbose_name='Powiązane pytanie otwarte' )
    related_question_closed = models.ManyToManyField(Question_Closed,  blank=True, default=None,
                                                related_name='related_yes_no_closed', verbose_name='Powiązane pytanie zamknięte')

    def __str__(self):
        return self.text
    class Meta:
        verbose_name = 'Pytanie przesiewowe'
        verbose_name_plural = 'Pytania przesiewowe'


class Answer_Closed(models.Model):
    question_closed = models.ForeignKey(Question_Closed, on_delete=models.CASCADE, related_name='answers_closed')

    text = models.CharField('Opcje', max_length=255)

    related_question_open = models.ManyToManyField(Question_Open,  blank=True, default=None,
                                                related_name='related_open', verbose_name='Powiązane pytanie otwarte')
    related_question_closed = models.ManyToManyField(Question_Closed,  blank=True, default=None,
                                                related_name='related_closed', verbose_name='Powiązane pytanie zamknięte')

    text_field = models.BooleanField('Wyświetl dodatkowe pole tekstowe', default=False)

    def __str__(self):
        return self.text

    class Meta:
        verbose_name = 'Odpowiedź do pytań zamkniętych'
        verbose_name_plural = 'Odpowiedzi do pytań zamkniętych'


class Message(models.Model):
    text= models.CharField('Komunikat', max_length=255)
    question_yes_no = models.ForeignKey(Question_Yes_No, on_delete=models.CASCADE, related_name='messages_yes_no', null=True, blank=True)
    class Meta:
        verbose_name = 'Komunikat'
        verbose_name_plural = 'Komunikaty'
