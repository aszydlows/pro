# Generated by Django 3.0.8 on 2020-09-20 19:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('formularz', '0015_auto_20200913_1306'),
    ]

    operations = [
        migrations.CreateModel(
            name='Question_Update',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.CharField(max_length=255, verbose_name='Pytanie otwarte')),
            ],
        ),
    ]
