# Generated by Django 3.0.8 on 2020-08-14 21:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('formularz', '0004_auto_20200814_2309'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='question_open',
            name='text_field',
        ),
        migrations.AddField(
            model_name='question_close',
            name='text_field',
            field=models.BooleanField(default=False, verbose_name='Wyświetl dodatkowe pole tekstowe'),
        ),
    ]
