from django import forms
from django.forms.widgets import RadioSelect, Textarea, CheckboxSelectMultiple


class ClosedQuestionForm(forms.Form):
    def __init__(self, question, *args, **kwargs):
        super(ClosedQuestionForm, self).__init__(*args, **kwargs)
        choice_list = [x for x in question.get_answers_list()]
        self.fields[question.text] = forms.MultipleChoiceField(choices=choice_list,
                                                   widget=CheckboxSelectMultiple, required=True)


class OpenQuestionForm(forms.Form):
    def __init__(self, question, *args, **kwargs):
        super(OpenQuestionForm, self).__init__(*args, **kwargs)
        self.fields[question.text] = forms.CharField(
            widget=Textarea(attrs={'style': 'width:100%'}))

MY_CHOICES = (
        ('Tak', 'Tak'),
        ('Nie', 'Nie'),

    )


class QuestionYesNoForm(forms.Form):
    def __init__(self, category, *args, **kwargs):
        super(QuestionYesNoForm, self).__init__(*args, **kwargs)
        for question in category.get_questions_list():
            self.fields[question[1]] = forms.ChoiceField(choices=MY_CHOICES, initial='', widget=RadioSelect, required=True)


class QuestionUpdate(forms.Form):
    def __init__(self, asked, *args, **kwargs):
        super(QuestionUpdate, self).__init__(*args, **kwargs)
        for (question, answer) in asked.items():
            self.fields[question] = forms.CharField(initial=answer, required = True, widget=Textarea(attrs={'style': 'width:100%'}))

