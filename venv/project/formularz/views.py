from django.shortcuts import render
from django import forms
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
import datetime
from .models import Part, Question_Closed, Question_Open, Answer_Closed, Question_Yes_No, Message, Yes_No_Category
from .forms import ClosedQuestionForm, OpenQuestionForm, QuestionYesNoForm, QuestionUpdate
from django.contrib import messages

from profil.models import User, Files, Student
from profil.views.teachers import add_test
from django.contrib import messages


# Create your views here.

class IndexForm(forms.Form):
    name = forms.CharField(label = "Imie")
    lastname = forms.CharField(label = "Nazwisko")
    age = forms.IntegerField(label = "Wiek", min_value=1, max_value=150)

    choices = User.objects.all().filter(is_teacher=True)
    aac = forms.ModelChoiceField(label="Koordynator AAC", queryset=choices)


parts_number=len(Part.objects.all())


def index(request):
    request.session["data"] = []
    request.session["open_main_counter"]=0
    request.session["answers"]={}
    request.session["asked"]=[]
    request.session["closed_main_counter"]=0
    request.session["related_open_counter"]=0
    request.session["related_closed_counter"]=0
    request.session["yes_no_counter"]=0
    request.session["yes_no"]=[]
    request.session["main_counter"]=0
    request.session["state"]="main_open"
    request.session["current_part"]=1
    request.session["update"]={}
    request.session["current_date"]=str(datetime.datetime.now().day) + "." + str(datetime.datetime.now().month) + "." + str(datetime.datetime.now().year) + "r."


    if request.method == "POST":
        form = IndexForm(request.POST)
        if form.is_valid():
            request.session["data"].append(form.cleaned_data["name"])
            request.session["data"].append(form.cleaned_data["lastname"])
            request.session["data"].append(form.cleaned_data["age"])
            request.session["data"].append(str(form.cleaned_data["aac"]))

            try:
                teacher = User.objects.get(username=str(form.cleaned_data["aac"]))
                student = Student.objects.get(owner=teacher, Imie=form.cleaned_data["name"],
                                              Nazwisko=form.cleaned_data["lastname"])

                return HttpResponseRedirect(
                    reverse("questions", kwargs={'question_id': request.session["main_counter"]}))
            except Student.DoesNotExist:
                messages.success(request, 'Wprowadź poprawne dane')



    return render(request, "formularz/index.html",{

        "current_date": request.session["current_date"],
        "form": IndexForm()
        })

def questions(request, question_id):
    current_question = None


    while( current_question == None and request.session["current_part"]<=parts_number):

        if request.session["state"] == "main_open":
            question_set=Question_Open.objects.filter(is_main = True, part = request.session["current_part"])

            try:
                current_question=question_set[request.session["open_main_counter"]]
                current_form=OpenQuestionForm
            except IndexError:
                request.session["state"] = "main_closed"
                request.session["open_main_counter"]=0


        if request.session["state"] == "main_closed":
            question_set=Question_Closed.objects.filter(is_main = True, part = request.session["current_part"])
            try:
                current_question=question_set[request.session["closed_main_counter"]]
                current_form=ClosedQuestionForm
            except IndexError:
                request.session["state"] = "main_open"
                request.session["closed_main_counter"]=0
                request.session["current_part"]=request.session["current_part"]+1

        if request.session["state"] == "yes_no":
            question_set = Question_Yes_No.objects.all()
            try:
                current_question = Yes_No_Category.objects.get(id=request.session["yes_no"][request.session["yes_no_counter"]])
                current_form = QuestionYesNoForm
            except IndexError:
                request.session["state"] = "main_closed"
                request.session["yes_no_counter"]=0
                request.session["yes_no"] = []


        if request.session["state"] == "related_open":
            try:
                current_question = Question_Open.objects.get(id=request.session["related_open"][request.session["related_open_counter"]])
                current_form=OpenQuestionForm

            except IndexError:
                request.session["state"] = "related_closed"
                request.session["related_open_counter"]=0
                request.session["related_open"]=[]


        if request.session["state"] == "related_closed":
            try:
                current_question = Question_Closed.objects.get(id=request.session["related_closed"][request.session["related_closed_counter"]])
                current_form=ClosedQuestionForm
            except IndexError:
                if request.session["yes_no"] == []:
                    request.session["state"] = "main_closed"
                else:
                    request.session["state"] = "yes_no"
                request.session["related_closed_counter"]=0



    if current_question == None :
        return HttpResponseRedirect(reverse("correct"))


    if request.method == "POST":

        mainform = current_form(current_question, request.POST)
        if mainform.is_valid():
            request.session["main_counter"]=request.session["main_counter"]+1

            if request.session["state"] == "main_open":
                request.session["asked"].append(str(current_question))
                answer = mainform.cleaned_data[current_question.text]
                request.session["open_main_counter"]=request.session["open_main_counter"]+1
                request.session["answers"][str(current_question)]=answer

            elif request.session["state"] == "main_closed":

                request.session["asked"].append(str(current_question))
                answers=mainform.cleaned_data
                answers_id=[]
                answer=''

                for pk in answers.get(current_question.text):
                    answers_id.append(int(pk))
                    answer=answer + Answer_Closed.objects.get(id=int(pk)).text + ", "
                answer=answer[:-2]

                request.session["answers"][str(current_question)]=answer

                if current_question.yes_no.all() is not None:
                    request.session["state"] = "yes_no"
                    request.session["yes_no_counter"]=0
                    for yes_no in current_question.yes_no.all():
                        request.session["yes_no"].append(yes_no.id)

                else:
                    request.session["yes_no"] = []

                request.session["related_open"]=[]
                request.session["related_closed"]=[]

                for answer_id in answers_id:

                    if Answer_Closed.objects.get(id=int(answer_id)).related_question_closed.all():
                        request.session["state"] = "related_closed"
                        for related in Answer_Closed.objects.get(id=int(answer_id)).related_question_closed.all():
                            request.session["related_closed"].append(related.id)

                    if Answer_Closed.objects.get(id=int(answer_id)).related_question_open.all():
                        request.session["state"] = "related_open"
                        for related in Answer_Closed.objects.get(id=int(answer_id)).related_question_open.all():
                            request.session["related_open"].append(related.id)


                request.session["closed_main_counter"]=request.session["closed_main_counter"]+1

            elif request.session["state"] == "yes_no":

                request.session["related_open"]=[]
                request.session["related_closed"]=[]
                request.session["yes_no_counter"] = request.session["yes_no_counter"]+1

                write_message=False

                for (question, answer) in mainform.cleaned_data.items():
                    request.session["asked"].append(question)
                    request.session["answers"][str(question)]=answer

                    if answer=="Tak":
                        if Question_Yes_No.objects.get(text=question).display_message==True:
                            write_message=True

                        for related in Question_Yes_No.objects.get(text=question).related_question_open.all():

                            if related.id not in request.session["related_open"]:
                                request.session["related_open"].append(related.id)

                        for related in Question_Yes_No.objects.get(text=question).related_question_closed.all():
                            if  related.id not in request.session["related_closed"]:
                                request.session["related_closed"].append(related.id)

                if write_message==True:
                     request.session["answers"][request.session["asked"][-1]]=request.session["answers"][request.session["asked"][-1]] + "\n \n" + current_question.message
                     messages.warning(request, current_question.message)
                     write_message=False

                if request.session["related_open"]:
                    request.session["state"] = "related_open"

                else:
                    request.session["state"] = "related_closed"




            elif request.session["state"] == "related_open":

                request.session["asked"].append(str(current_question))
                answer = mainform.cleaned_data[current_question.text]
                request.session["related_open_counter"]=request.session["related_open_counter"]+1
                request.session["answers"][str(current_question)]=answer

            elif request.session["state"] == "related_closed":

                request.session["related_closed_counter"]=request.session["related_closed_counter"]+1

                request.session["asked"].append(str(current_question))
                answers=mainform.cleaned_data
                answers_id=[]
                answer=''

                for pk in answers.get(current_question.text):
                    answers_id.append(int(pk))
                    answer=answer + Answer_Closed.objects.get(id=int(pk)).text + ", "
                answer=answer[:-2]

                request.session["answers"][str(current_question)]=answer

                if current_question.yes_no is not None:
                    request.session["state"] = "yes_no"
                    request.session["yes_no_counter"] = int(current_question.yes_no.id)


                for answer_id in answers_id:
                    if Answer_Closed.objects.get(id=int(answer_id)).related_question_open.all():
                        request.session["state"] = "related_open"
                        for related in Answer_Closed.objects.get(id=int(answer_id)).related_question_open.all():
                            request.session["related_open"].append(related.id)

                    if Answer_Closed.objects.get(id=int(answer_id)).related_question_closed.all():
                        request.session["state"] = "related_closed"
                        for related in Answer_Closed.objects.get(id=int(answer_id)).related_question_open.all():
                            request.session["related_open"].append(related.id)


            return HttpResponseRedirect(reverse("questions", kwargs={'question_id' : request.session["main_counter"]}))


        else:
            try:
                pk=int(request.path.split('/')[-1])
                back_question=Question_Open.objects.get(text=request.session["asked"][pk])
                mainform = OpenQuestionForm(back_question, request.POST)

                if mainform.is_valid():
                    request.session["update"]={}
                    request.session["update"][str(back_question)] = mainform.cleaned_data[back_question.text]
                    request.session["answers"].update(request.session["update"])

            except IndexError:
                pass

            return HttpResponseRedirect(reverse("questions", kwargs={'question_id' : request.session["main_counter"]}))

    return render(request, "formularz/main_questions.html",{
        "form": current_form(current_question),
        "part": Part.objects.get(part_nr=request.session["current_part"])

        })



def form_done(request):

    request.quiz = request.session["answers"]
    add_test(request, request.session["data"], request.session["current_date"])

    return render(request, "formularz/form_done.html", {
        "questions": request.session["answers"].items(),

    })

def correct(request):


    allow = {}


    for key, value in request.session["answers"].items():
        if Question_Open.objects.filter(text=key).exists():
            allow[key] = value


    form = QuestionUpdate(allow)
    if request.method == "POST":
        mainform = QuestionUpdate(allow, request.POST)
        if mainform.is_valid():
            request.session["update"] = mainform.cleaned_data
            request.session["answers"].update(request.session["update"])

        return HttpResponseRedirect(reverse("form_done"))



    return render(request, "formularz/correct.html", {
        "form": form,

    })

