from django.urls import path
from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("<int:question_id>", views.questions, name="questions"),
    path("correct", views.correct, name="correct"),
    path("form_done", views.form_done, name="form_done"),

]
