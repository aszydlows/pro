from django.contrib import admin

from .models import Part, Question_Closed, Question_Open, Answer_Closed, Question_Yes_No, Message, Yes_No_Category

import nested_admin


class AnswerInLine_Closed(nested_admin.NestedTabularInline):
    model = Answer_Closed
    extra = 1
    fk_name = 'question_closed'

class MessageInLine_Yes_No(nested_admin.NestedTabularInline):
    model = Message
    extra = 0
    max_num = 1

class QuestionInline_Open(nested_admin.NestedTabularInline):
    model = Question_Open
    extra = 1

class QuestionInline_Yes_No(nested_admin.NestedTabularInline):
    model = Question_Yes_No
    inlines = [MessageInLine_Yes_No]
    extra = 1

class QuestionInline_Closed(nested_admin.NestedTabularInline):
    model = Question_Closed
    inlines = [AnswerInLine_Closed]
    extra = 1


class QuizAdmin(nested_admin.NestedModelAdmin):
    inlines = [QuestionInline_Closed,QuestionInline_Open,QuestionInline_Yes_No,]

class Question_ClosedAdmin(nested_admin.NestedModelAdmin):
    inlines = [AnswerInLine_Closed]


class Question_Yes_NoAdmin(nested_admin.NestedModelAdmin):
   # inlines = [MessageInLine_Yes_No]
   pass


admin.site.register(Part)
admin.site.register(Yes_No_Category)
admin.site.register(Question_Closed,Question_ClosedAdmin )
admin.site.register(Question_Open)
admin.site.register(Question_Yes_No, Question_Yes_NoAdmin)
